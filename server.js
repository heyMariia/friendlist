import express from 'express';
import bodyParser from 'body-parser';

let app = express();
let port = process.env.PORT || 5000;

let BASE_ID = new Date().getTime(); // use current time in milliseconds as initial id

function isEmptyObject(obj) {
    for(let prop in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, prop)) {
            return false;
        }
    }
    return true;
}

class Friend {

    constructor(name, comment, starred, id=undefined){
        if (id === undefined || id === null){
            BASE_ID += 1;
            id = BASE_ID;
        }
        this.id = id
        this.name = name || '';
        this.comment = comment || '';
        this.starred = Boolean(starred);
    }
    validate_field(field_name, data){
        let validators = {
            'name': (data) => {
                if (data === undefined){
                    return 'No data'
                }
                if (!(typeof data === 'string' || data instanceof String)) {
                    return 'Wrong format, string required'
                }
                if (data.length === 0){
                    return 'Name can not be empty';
                }
                if (data.length > 20){
                    return 'Name can not be longer then 20 characters';
                }
                return;
            },
            'comment': (data) => {
                if (data === undefined){
                    return 'No data'
                }
                if (!(typeof data === 'string' || data instanceof String)) {
                    return 'Wrong format, string required'
                }
                if (data.length > 20){
                    return 'Comment can not be longer then 20 characters';
                }
                return;
            }
        }

       let validator = validators[field_name]
       if (validator) {
            return validator(data)
       }
       return;
    }
    validate(){
        let errors = {};
        let name_error = this.validate_field('name',this.name)
        let comment_error = this.validate_field('comment',this.comment)
        if (name_error) {
            errors.name = name_error
        }
        if (comment_error) {
            errors.comment = comment_error
        }
        return errors;
    }

    static from_json(json){
        return new Friend(json.name, json.comment, json.starred)
    }
}

class API {
    constructor(){
        this.all_friends = [];
        this.load_friends();
    }

    load_friends(){
        this.all_friends.push(new Friend("friend 1", "comment 1", false, 1))
    }

    save_friends(){
        //write this.all_friends to file
    }

    _by_id(id){
        return this.all_friends.find((friend) => {
            return id == friend.id;
        })
    }

    get_friends_list = (req, res) => {
        setTimeout(() => {
            res.json({
                friends: this.all_friends
            })
        }, 3000 );

    }


    get_friend_by_id = (req, res) => {
        let friend = this._by_id(req.params.friendId);
        if (friend == undefined) {
            res.status(404);
            res.json({ error: 'No friend with such id' });
        } else {
            res.json(friend);
        }
    }

    add_friend = (req, res) => {
        let friend = Friend.from_json(req.body);
        let errors = friend.validate();
        if (isEmptyObject(errors)) {
            this.all_friends.push(friend);
            res.json(friend);
        } else {
            res.status(400);
            res.json({ error: 'wrong data', details: errors })
        }
    }

    update_friend = (req, res) => {
        console.log(req.body);
        let friend = this._by_id(req.params.friendId);
        if (friend == undefined) {
            res.status(404);
            res.json({ error: 'No friend with such id' });
        } else {
            let errors = {}
            let props = Object.getOwnPropertyNames(req.body)
            console.log('props')
            console.log(props)
            for (let field_name of props) {
                let error = friend.validate_field(field_name, req.body[field_name])
                console.log('field_name, value, error')
                console.log(field_name, req.body[field_name], error)
                if (error) {
                    errors[field_name] = error
                }
            }

            if (isEmptyObject(errors)) {
                for (let field_name of props) {
                    friend[field_name] = req.body[field_name]
                }
                res.json(friend);
            } else {
                res.status(400);
                res.json({ error: 'wrong data', details: errors })
            }
        }
    }

    delete_friend = (req, res) => {
        let friend_to_delete = this._by_id(req.params.friendId);
        if (friend_to_delete == undefined) {
            res.status(404);
            res.json({ error: 'No friend with such id' });
        } else {
            this.all_friends = this.all_friends.filter(function(friend) {
                return friend_to_delete.id !== friend.id;
            });
            res.status(204);
            res.json({})
        }
    }
}

let api = new API();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.route('/api/friends')
    .get(api.get_friends_list)
    .post(api.add_friend);

app.route('/api/friends/:friendId')
    .get(api.get_friend_by_id)
    .patch(api.update_friend)
    .delete(api.delete_friend);

app.listen(port);

console.log('RESTful API server started on: ' + port);