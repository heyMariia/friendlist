import React, { Component} from 'react';
import PropTypes from 'prop-types';
import './AddCommentInput.css';

export default class AddCommentInput extends Component {
    static propTypes = {
        textChanged: PropTypes.func.isRequired,
        comment: PropTypes.string.isRequired,
    }
ye
    render () {
        let {comment} = this.state
        return (
                  <textarea
                      type="text"
                      autoFocus="true"
                      className='form-control'
                      placeholder="Type the comment"
                      value={comment}
                      onChange={this.handleChange}
                      onKeyDown={this.handleSubmit} />

        );
    }

    componentWillReceiveProps(nextProps) {
        // this.setState(nextProps.comment)
        this.setState({comment : nextProps.comment})

    }


    constructor (props, context) {
        super(props, context);
        this.state = {
            comment: props.comment || '',
        };
    }
    handleChange = (e) => {
        this.setState({ comment: e.target.value });
    }

    handleSubmit  = (e) => {
        const comment = e.target.value.trim();
        if (e.which === 13) {
            this.props.textChanged(comment);
            this.setState({ comment: ''});
        }
        // console.log('comment')
        // console.log(comment)
    }

}
