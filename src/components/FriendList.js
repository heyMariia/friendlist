import React, {Component} from 'react';
import PropTypes from 'prop-types';

import styles from './FriendList.css';
import FriendListItem from './FriendListItem';

export default class FriendList extends Component {
    static propTypes = {
        friends: PropTypes.object.isRequired,
        actions: PropTypes.object.isRequired
    }


    render() {

        let items = []
        for (let friendId in this.props.friends) {
            let friend = this.props.friends[friendId]
            items.push(<FriendListItem
                key={friend.id}
                id={friend.id}
                name={friend.name}
                starred={friend.starred}
                deleted={friend.deleted}
                comment={friend.comment}
                {...this.props.actions}
            />)
        }
        return (
            <ul className={styles.friendList}>
                {items}
            </ul>
        );
    }

}
