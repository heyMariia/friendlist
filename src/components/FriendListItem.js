import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styles from './FriendListItem.css';
import Star from 'react-icons/lib/fa/star-o';
import Trash from 'react-icons/lib/fa/trash';
import {Link} from "react-router";


class FriendListItem extends Component {
    static propTypes = {
        id: PropTypes.number.isRequired,
        comment: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        starred: PropTypes.bool,
        deleted: PropTypes.bool,
        // starFriend: PropTypes.func.isRequired,
        onTrashClick: PropTypes.func,
        pickFriend: PropTypes.func.isRequired
    }

    static defaultProps = {
        starred: false,
        deleted: false,
    }
    // onClick = (e) => {
    //     let func = this.props.pickFriend
    //     if (typeof func === 'function') {
    //         func(this.props.id)
    //     }
    //     // this.props.history.push(`/${this.props.id}`);
    // }
    // deleteHandler(i, e) {
    //     e.preventDefault();
    //     this.props.onDelete(this.props.blogPosts[i].id);
    // };

    render() {
        return (
            <div>
                <Link to={`/${this.props.id}`}>
                    <li className={this.props.deleted ? styles.friendListItemDeleted : ''} >
                        <div className={this.props.starred ? styles.friendListItemActive : styles.friendListItem}>
                            <div className={styles.friendInfos}>
                                <div><span>{this.props.name}</span></div>
                                <div><span>{this.props.comment}</span></div>
                            </div>
                            <div className={styles.friendActions}>
                                <button className={`btn btn-default ${styles.btnAction}`}
                                        onClick={() => this.props.starFriend(this.props.id)}>
                                    <i>
                                        <Star/>
                                    </i>
                                    {/*<i className={'fa' + (this.props.starred ? ' fa-star': ' fa-star-o')}/>*/}
                                </button>
                                <button className={`btn btn-default ${styles.btnAction}`}

                                          onClick={() => this.props.deleteFriend(this.props.id)}>
                                    <i>
                                        <Trash/>
                                    </i>
                                </button>
                            </div>
                        </div>
                    </li>
                </Link>

            </div>

        );
    }

}

export default FriendListItem;
