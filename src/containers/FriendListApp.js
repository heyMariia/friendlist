import React, {Component} from 'react';
import styles from './FriendListApp.css';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import * as FriendsActions from '../actions/FriendsActions';
import {FriendList, AddFriendInput} from '../components';
import AddCommentInput from "../components/AddCommentInput";
import DevTools from "./DevTools";

function mapStateToProps(state, ownProps) {
    return {
        friendlist: {
            ...state.friendlist,
            pickedFriendId: ownProps.params.friendId,
        }
    }
}


class FriendListApp extends Component {

    static propTypes = {
        friendlist: PropTypes.object.isRequired,
        dispatch: PropTypes.func.isRequired,
        // isLoading: PropTypes.bool.isRequired,

    };

    constructor() {
        super();
    }

    render() {
        const {friendlist: {friendsById, pickedFriendId}, dispatch} = this.props;
        const actions = bindActionCreators(FriendsActions, dispatch);
        let pickedFriend = friendsById[pickedFriendId];
        console.log('pickedFriend')
        console.log(pickedFriend)
        // const {isloading, error} = this.props.friendlist;
        // if (isloading) {
        //     return <div className={styles.friendListApp}>
        //         <h1>The FriendList</h1>
        //         <AddFriendInput addFriend={actions.addFriend}/>
        //             <div className={styles.friendListApp}>
        //                 <h1>Loading...</h1>
        //             </div>
        //         </div>
        //         } else if(error) {
        //         return <div className="alert alert-danger">Error: {error.message}</div>
        //     }
                return (
                <div>
                    <div className={styles.friend}>
                        <div className={styles.friendListApp}>
                            <h1>The FriendList</h1>
                            <AddFriendInput addFriend={actions.addFriend}/>
                            <FriendList friends={friendsById} actions={actions}/>
                        </div>
                        {pickedFriend &&
                        <div className={styles.commentListApp}>
                            <h1>Comments</h1>
                            <AddCommentInput textChanged={(comment) => actions.addComment(comment, pickedFriend.id)}
                                             comment={pickedFriend.comment}/>
                        </div>
                        }
                    </div>
                    <DevTools/>
                </div>
                );
                }
                }

                export default connect(mapStateToProps)(FriendListApp)
