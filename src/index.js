// import React from 'react';
// import ReactDOM from 'react-dom';
// import './index.css';
import FriendListApp from "./containers/FriendListApp";
import registerServiceWorker from './registerServiceWorker';

import React from 'react'
import ReactDOM from 'react-dom'

import { createStore, combineReducers, applyMiddleware, compose} from 'redux'
import { Provider } from 'react-redux'
import reducers from "./reducers/index";
import DevTools from "./containers/DevTools"; // Or wherever you keep your reducers

import {Router, browserHistory, Redirect, Route} from 'react-router'
import { syncHistoryWithStore, routerReducer } from 'react-router-redux'
import thunk from "redux-thunk";
import * as FriendsActions from "./actions/FriendsActions";
import {deleteFriend} from "./actions/FriendsActions";



const enhancer = compose(
    DevTools.instrument(),

);

const store = createStore(
    combineReducers({
        ...reducers,
        routing: routerReducer,
    }),
    enhancer,
    applyMiddleware (thunk)
)
store.dispatch(FriendsActions.getFriends());

// store.dispatch(FriendsActions.addFriends());
const history = syncHistoryWithStore(browserHistory, store);

ReactDOM.render(
    <Provider store={store}>
        <Router history={history}  >
            <Route path="/" component={FriendListApp}>
            <Route path=":friendId" component={FriendListApp}/>
        </Route>
        </Router>
    </Provider>,
    document.getElementById('root')
);
registerServiceWorker();


// const store = Store();
// const history = createHistory()
//
// ReactDOM.render(
//     <Provider store={store}>
//         <Router history={history}>
//             <Route path="/" component={App}/>
//         </Router>
//     </Provider>,
//     document.getElementById('root')
// );

