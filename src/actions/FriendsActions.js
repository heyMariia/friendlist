import * as types from '../constants/ActionTypes';

import {GET_FRIENDS_SUCCESS} from "../constants/ActionTypes";
import axios from 'axios';
import {ADD_FRIEND} from "../constants/ActionTypes";
import {ADD_FRIEND_SUCCESS} from "../constants/ActionTypes";
import {ADD_FRIEND_ERROR} from "../constants/ActionTypes";
import {GET_FRIENDS} from "../constants/ActionTypes";
import {GET_FRIENDS_ERROR} from "../constants/ActionTypes";


// export function getFriends () {
//     return dispatch => {
//         dispatch(getFriendsLoading());
//         axios.get('/api/friends')
//             .then(response => {
//                 dispatch(getFriendsSuccess(response.data.friends))
//             })
//             .catch(error => {
//                 dispatch(getFriendsError(error))
//             })
//     }
// };

// export function getFriendsLoading() {
//     return {
//         type: GET_FRIENDS,
//     };
// }
export function getFriends() {
    return function(dispatch) {
        return axios.get('/api/friends').then(response => {
            dispatch(getFriendsSuccess(response.data.friends));
        })
            .catch(error => {
                dispatch({type: "GET_FRIEND_ERROR", payload: error});
            });
    };
}
export function getFriendsSuccess(friends) {
    return {
        type: GET_FRIENDS_SUCCESS,
        friends
    }
}

export function addFriend(name) {
    return function (dispatch)  {
        return axios.post('/api/friends' , {name:name} ).then(response => {
            dispatch({type: 'ADD_FRIEND', payload: response.data});
        }).catch(error => {
            dispatch({type: "ADD_FRIEND_ERROR", payload: error});
        });
    }
}



export function deleteFriend(id) {
    return function (dispatch)  {
        return axios.delete('/api/friends/' +id)
            .then(response => {
            dispatch({type: 'DELETE_FRIEND', id: id});

        }).catch(error => {
            dispatch({type: "DELETE_FRIEND_ERROR", payload: error});
        });
    }
}

export function starFriend(id) {
return {
    type: types.STAR_FRIEND,
    id
};
}
export function addComment(comment, id)  {
    return function (dispatch)  {
        return axios.patch('/api/friends/' + id , {comment} ).then(response => {
            console.log('addComment response')
            console.log(response)
            dispatch({type: 'COMMENT_FRIEND', comment: response.data.comment,  id: response.data.id});
        }).catch(error => {
            dispatch({type: "COMMENT_FRIEND_ERROR", payload: error});
        });
    }
}
// export function addComment(comment, id) {
//     return {
//         type: types.COMMENT_FRIEND,
//         comment,
//         id
//     };
// }
export function pickFriend(id) {
    return {
        type: types.PICK_FRIEND,
        id
    };
}

// export function changeFriendInfo(id) {
//     return function (dispatch)  {
//         return axios.patch('/api/friends/' +id).then(response => {
//             dispatch({type: 'CHANGE_INFO', payload: response.data});
//         }).catch(error => {
//             dispatch({type: "CHANGE_INFO_ERROR", payload: error});
//         });
//     }
// }

// export function requestPosts(json) {
//     return {
//         type: REQUEST_POSTS,
//         json
//     }
// }
