import * as types from '../constants/ActionTypes';
import {mapValues, omit, assign} from 'lodash';
import {GET_FRIENDS_SUCCESS} from "../constants/ActionTypes";
import {ADD_FRIEND} from "../constants/ActionTypes";
import {DELETE_FRIEND} from "../constants/ActionTypes";
import {ADD_FRIEND_SUCCESS} from "../constants/ActionTypes";
import {GET_FRIENDS} from "../constants/ActionTypes";
import {GET_FRIENDS_ERROR} from "../constants/ActionTypes";
// import omit from 'lodash/object/omit';
// import assign from 'lodash.assign';
// import mapValues from 'lodash/object/mapValues';

const initialState = {
    friends: [],
    friendsById: {},
    pickedFriendId: undefined,
    isloading:false,
    error:null
};
let mapFriendsById = (friends) => {
    let mapping = {};
    console.log('friends');
    console.log(friends);
    for (let i = 0; i < friends.length; i++) {
        let friend = friends[i];
        mapping[friend.id] = friend;
    }
    return mapping;
};

export default (state = initialState, action) => {
    switch (action.type) {
        // case GET_FRIENDS:
        //     return{
        //         ...state,
        //         isloading: true
        //     };
        case GET_FRIENDS_SUCCESS:
            return {
                ...state,
                friends: action.friends,
                friendsById: mapFriendsById(action.friends),

            };

        case ADD_FRIEND:
            console.log('ADD_FRIEND');
            console.log(action);
            let new_friends = [...state.friends, action.payload];
            console.log('new_friends');
            console.log(new_friends);
            return {
                ...state,
                friends: new_friends,
                friendsById: mapFriendsById(new_friends),
            };


        case types.DELETE_FRIEND:
            console.log('DELETE_FRIEND');
            console.log(action);
            console.log(state.friends);
             let friend_to_delete = state.friends.filter(friends => friends.id !== action.id)
            console.log(friend_to_delete);
            return {
                ...state,
                friends: friend_to_delete,
                friendsById: mapFriendsById(friend_to_delete)
            };
                case types.STAR_FRIEND:
            return {
                ...state,
                friendsById: mapValues(state.friendsById, (friend) => {
                    return friend.id === action.id ?
                        assign({}, friend, {starred: !friend.starred}) :
                        friend
                })
            };
        case types.COMMENT_FRIEND:
            console.log('COMMENT_FRIEND');
            console.log(action);
            return {
                ...state,
                friendsById: mapValues(state.friendsById, (friend) => {
                    return friend.id === action.id ?
                        assign({}, friend, {comment: action.comment}) :
                        friend
                })
            };
        case types.PICK_FRIEND:
            return {
                ...state,
                pickedFriendId: action.id,
            };
        default:
            return state;
    }
};


// export default function friends(state = initialState, action) {
//
//
//
//     switch (action.type) {
//
//         case types.ADD_FRIEND:
//             const newId = state.friends[state.friends.length - 1] + 1;
//             return {
//                 ...state,
//                 friends: state.friends.concat(newId),
//                 friendsById: {
//                     ...state.friendsById,
//                     [newId]: {
//                         id: newId,
//                         name: action.name
//                     }
//                 },
//             }
//
//         case types.DELETE_FRIEND:
//           return {
//             ...state,
//             friends: state.friends.filter(id => id !== action.id),
//             friendsById: omit(state.friendsById, action.id)
//           }
//
//         case types.STAR_FRIEND:
//             return {
//                 ...state,
//                 friendsById: mapValues(state.friendsById, (friend) => {
//                     return friend.id === action.id ?
//                         assign({}, friend, {starred: !friend.starred}) :
//                         friend
//                 })
//             }
//
//         //
//         // case types.DELETE_FRIEND:
//         //     return {
//         //         ...state,
//         //         friendsById: mapValues(state.friendsById, (friend) => {
//         //             return friend.id === action.id ?
//         //                 assign({}, friend, {deleted: !friend.deleted}) :
//         //                 friend
//         //         })
//         //     }
//
//         case types.COMMENT_FRIEND:
//             return {
//                 ...state,
//                 friendsById: mapValues(state.friendsById, (friend) => {
//                     return friend.id === action.id ?
//                         assign({}, friend, {comment: action.comment}) :
//                         friend
//                 })
//             }
//
//         case types.PICK_FRIEND:
//             return {
//                 ...state,
//                 pickedFriendId: action.id,
//             }
//
//         default:
//             return state;
//     }
// }
// const initialState = {
//     friends: [1, 2, 3],
//     pickedFriendId: undefined,
//     friendsById: {
//         1: {
//             id: 1,
//             name: 'Theodore Roosevelt',
//             comment: 'test1'
//         },
//         2: {
//             id: 2,
//             name: 'Abraham Lincoln',
//             comment: ''
//         },
//         3: {
//             id: 3,
//             name: 'George Washington',
//             comment: ''
//         }
//     }
// };
