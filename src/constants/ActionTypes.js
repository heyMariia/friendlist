
export const STAR_FRIEND = 'STAR_FRIEND';

export const COMMENT_FRIEND = 'COMMENT_FRIEND';
export const COMMENT_FRIEND_ERROR = 'COMMENT_FRIEND_ERROR';
export const CHANGE_INFO_ERROR = 'CHANGE_INFO_ERROR';
export const CHANGE_INFO = 'CHANGE_INFO';
export const PICK_FRIEND = 'PICK_FRIEND';
export const FETCH_POSTS = 'FETCH_POSTS';


//get friend
export const GET_FRIENDS = 'GET_FRIENDS'
export const GET_FRIENDS_SUCCESS = 'GET_FRIENDS_SUCCESS'
export const GET_FRIENDS_ERROR = 'GET_FRIENDS_ERROR'







//add friend
export const ADD_FRIEND = 'ADD_FRIEND';
export const  ADD_FRIEND_SUCCESS = ' ADD_FRIEND_SUCCESS'
export const ADD_FRIEND_ERROR = 'ADD_FRIEND_ERROR'



//delete friend
export const DELETE_FRIEND = 'DELETE_FRIEND';
export const DELETE_FRIEND_SUCCESS = 'DELETE_FRIEND_SUCCESS'
export const DELETE_FRIEND_ERROR = 'DELETE_FRIEND_ERROR'